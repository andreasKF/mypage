# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table project_type (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  constraint pk_project_type primary key (id)
);


# --- !Downs

drop table if exists project_type;

