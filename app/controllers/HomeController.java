package controllers;

import play.mvc.*;

import javax.inject.*;
import javax.persistence.*;
import java.util.concurrent.*;

public class HomeController extends Controller {




    //https://www.playframework.com/documentation/2.8.x/JavaJPA
    //https://mvnrepository.com/artifact/org.postgresql/postgresql
    public Result index() {
        return ok(views.html.index.render());
    }

}