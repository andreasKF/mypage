package controllers;


import models.UserType;
import play.mvc.*;

import javax.inject.*;
import javax.persistence.*;

public class AdminController extends Controller {

    public Result index() {
        return ok(views.html.index.render());
    }

    public static UserType CheckToken(String token) {
        UserType user = UserType.find.where().eq("token", token).findUnique();
        return user;
    }
}