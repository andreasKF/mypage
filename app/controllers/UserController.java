package controllers;


import play.mvc.*;

import javax.inject.*;
import javax.persistence.*;

public class UserController extends Controller {

    public Result index() {
        return ok(views.html.index.render());
    }

}