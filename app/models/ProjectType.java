package models;

import io.ebean.Finder;
import play.data.validation.Constraints;
import javax.persistence.Entity;
import javax.persistence.Id;


import static play.mvc.Results.ok;

@Entity
public class ProjectType extends BaseModel {

    private static final long serialVersionUID = 1L;

    @Id
    @Constraints.Required
    public String name1;

    public String name2;



    public static Finder<Long,ProjectType> find = new Finder<Long,ProjectType>(ProjectType.class);
}