package models;

import io.ebean.Model;
import javax.persistence.*;

@MappedSuperclass
public class BaseModel extends Model {

    //@GeneratedValue (strategy = GenerationType.IDENTITY)
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    public Long id;

}