package models;

import io.ebean.Finder;
import play.data.validation.Constraints;
import javax.persistence.*;

@Entity
public class UserType extends BaseModel {

    private static final long serialVersionUID = 1L;

    @Id
    Long id;

    @Constraints.Required
    public String name;

    @Constraints.Required
    public String pw;

    @Constraints.Required
    @Constraints.Email(message="Please provide a valid email address")
    public String mail;

    // <editor-fold desc="getter setter">
        public static long getSerialVersionUID() {
            return serialVersionUID;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPw() {
            return pw;
        }

        public void setPw(String pw) {
            this.pw = pw;
        }

        public String getMail() {
            return mail;
        }

        public void setMail(String mail) {
            this.mail = mail;
        }
    // </editor-fold>

    /*
    public static List<UserType> GetUser(String mail){
        List<UserType> user = UserType.find.where().orderBy("mail").findUnique();

        //UserType user2 = UserType.find.byId(1L);
        return user;
    }/**/

    public static Finder<Long,UserType> find = new Finder<Long,UserType>(UserType.class);
}