name := """meineSeite"""
organization := "com.af86"

version := "1.0-SNAPSHOT"

lazy val root = (project in file("."))
  .enablePlugins(PlayJava, PlayEbean)
  .settings(
    name := "play-java-ebean-example",
    version := "1.0.0-SNAPSHOT",
    scalaVersion := "2.13.3",
    libraryDependencies ++= Seq(
      guice,
      jdbc,
      javaJdbc,
      javaJpa,
      javaCore,
      evolutions,
      "org.postgresql" % "postgresql" % "9.4.1212",
      "javax.xml.bind"     % "jaxb-api"     % "2.3.1",
      "javax.activation"   % "activation"   % "1.1.1",
      "org.glassfish.jaxb" % "jaxb-runtime" % "2.3.2",
    )
  )

scalaVersion := "2.13.3"

//PlayKeys.externalizeResourcesExcludes += baseDirectory.value / "conf" / "META-INF" / "persistence.xml0"
